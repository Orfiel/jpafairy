package orfiel;
import javax.persistence.*;

@Entity
@Table(name = "fairies")
public class Fairy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private int wingspan;

    @Column
    private int powderAmount;

    @Column
    private String name;

    public Fairy() {
    }

    public Fairy(int wingspan, int powderAmount, String name) {
        this.wingspan = wingspan;
        this.powderAmount = powderAmount;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWingspan() {
        return wingspan;
    }

    public void setWingspan(int wingspan) {
        this.wingspan = wingspan;
    }

    public int getPowderAmount() {
        return powderAmount;
    }

    public void setPowderAmount(int powderAmount) {
        this.powderAmount = powderAmount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
