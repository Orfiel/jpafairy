CREATE USER 'fairies' IDENTIFIED BY 'm4g35m4g35';

CREATE DATABASE fairies CHARACTER SET utf8 COLLATE utf8_general_ci;

GRANT ALL ON fairies.* TO 'fairies';

Use fairies;

CREATE TABLE fairies (
    id INT NOT NULL AUTO_INCREMENT,
    wingspan INT,
    powderAmount INT,
    name VARCHAR(256),
    PRIMARY KEY (id)
);